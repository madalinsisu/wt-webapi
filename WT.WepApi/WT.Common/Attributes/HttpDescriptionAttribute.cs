﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WT.Common.Attributes
{
    public class HttpDescriptionAttribute: DescriptionAttribute
    {
        public int HttpStatusCode { get; set; }
        public HttpDescriptionAttribute(): base()
        {
        }

        public HttpDescriptionAttribute(string description) : base(description)
        {
        }

        public HttpDescriptionAttribute(string description, int httpStatusCode): this(description)
        {
            HttpStatusCode = httpStatusCode;
        }
    }
}
