﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WT.Common.Constants
{
    public class Constants
    {
        public class DbSettings
        {
            public static string iPortalCrmType = "WTLoginType";
        }

        public class Messages
        {
            public static string ExceptionUnauthorized = "You are not authorized to access the API!";
            public static string ExceptionDivideByZero = "Divide by zero occurred!";
            public static string ExceptionDatabase = "Database error!";
            public static string ExceptionNotImplemented = "Not implemented exception!";
            public static string ExceptionNullReference = "Null reference exception occurred!";
            public static string ExceptionTimeout = "Timeout exception occured!";
            public static string ExceptionBadRequest = "Invalid request!";
            public static string ExceptionUniqueConstraint = "One of the properties is marked as Unique index and there is already an entry with that value.";
            public static string ExceptionInvalidCredentials = "Incorrect username or password";
            public static string ExceptionInvalidEmailOrUsername = "Email or username already in use";
            public static string ExceptionPassword = "Password does not match confirm password";
        }
    }
}
