﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WT.Common.Constants
{
    public class EnumDescriptions
    {
        public const string GenericError = "One or more errors occured";
        public const string Success = "Success";
        public const string InvalidUsernameOrEmail = "Username or email already taken by another user";
        public const string WrongOldPassword = "Your current password is not correct";
        public const string PasswordNotMatches = "Your new password doesn't match the confirmation password";
        public const string UserNotFound = "The user was not found";
    }
}
