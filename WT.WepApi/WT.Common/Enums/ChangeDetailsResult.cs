﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WT.Common.Attributes;
using WT.Common.Constants;

namespace WT.Common.Enums
{
    public enum ChangeDetailsResult
    {
        [HttpDescriptionAttribute(EnumDescriptions.GenericError, HttpStatusCode = (int)HttpStatusCode.BadRequest)]
        None = 0,

        [HttpDescriptionAttribute(EnumDescriptions.Success, HttpStatusCode = (int)HttpStatusCode.OK)]
        Success,

        [HttpDescriptionAttribute(EnumDescriptions.InvalidUsernameOrEmail, HttpStatusCode = (int)HttpStatusCode.BadRequest)]
        InvalidUsernameOrEmail
    }
}
