﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WT.Common.Attributes;
using WT.Common.Constants;

namespace WT.Common.Enums
{
    public enum ChangePasswordResult
    {
        [HttpDescriptionAttribute(EnumDescriptions.GenericError, HttpStatusCode = (int)HttpStatusCode.BadRequest)]
        None = 0,

        [HttpDescriptionAttribute(EnumDescriptions.Success, HttpStatusCode = (int)HttpStatusCode.OK)]
        Success,

        [HttpDescriptionAttribute(EnumDescriptions.WrongOldPassword, HttpStatusCode = (int)HttpStatusCode.BadRequest)]
        WrongOldPassword,

        [HttpDescriptionAttribute(EnumDescriptions.PasswordNotMatches, HttpStatusCode = (int)HttpStatusCode.BadRequest)]
        NotMatch,

        [HttpDescriptionAttribute(EnumDescriptions.UserNotFound, HttpStatusCode = (int)HttpStatusCode.BadRequest)]
        UserNotFound
    }
}
