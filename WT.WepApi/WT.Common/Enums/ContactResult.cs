﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WT.Common.Attributes;
using WT.Common.Constants;

namespace WT.Common.Enums
{
    public enum ContactResult
    {
        [HttpDescriptionAttribute(EnumDescriptions.GenericError, HttpStatusCode = (int)HttpStatusCode.BadRequest)]
        None = 1,

        [HttpDescriptionAttribute(EnumDescriptions.Success, HttpStatusCode = (int)HttpStatusCode.OK)]
        Success,

        [HttpDescriptionAttribute(EnumDescriptions.GenericError, HttpStatusCode = (int)HttpStatusCode.BadRequest)]
        Failed
    }
}
