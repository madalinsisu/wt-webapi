﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WT.Common.Attributes;
using WT.Common.Model;

namespace WT.Common.Helpers
{
    public class EnumHelper
    {
        public static HttpEnumResult GetDescription<T>(T e) where T : IConvertible
        {
            HttpEnumResult result = new HttpEnumResult();

            if (e is Enum)
            {
                Type type = e.GetType();
                Array values = System.Enum.GetValues(type);

                foreach (int val in values)
                {
                    if (val == e.ToInt32(CultureInfo.InvariantCulture))
                    {
                        var memInfo = type.GetMember(type.GetEnumName(val));
                        var descriptionAttributes = memInfo[0].GetCustomAttributes(typeof(HttpDescriptionAttribute), false);
                        if (descriptionAttributes.Length > 0)
                        {
                            result = new HttpEnumResult()
                            {
                                Description = ((HttpDescriptionAttribute)descriptionAttributes[0]).Description,
                                HttpStatusCode = ((HttpDescriptionAttribute)descriptionAttributes[0]).HttpStatusCode
                            };

                            break;
                        }
                    }
                }
            }
            return result;
        }
    }
}
