﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WT.Common.Model
{
    public class HttpEnumResult
    {
        public string Description { get; set; }
        public int HttpStatusCode { get; set; }
    }
}
