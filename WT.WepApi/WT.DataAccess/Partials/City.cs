﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WT.ModelAbstractions;

namespace WT.DataAccess.Models
{
    public partial class City: ICity
    {
        IState ICity.State
        {
            get { return this.State; }
        }
    }
}
