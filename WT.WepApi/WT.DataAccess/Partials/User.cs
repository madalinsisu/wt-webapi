﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WT.ModelAbstractions;

namespace WT.DataAccess.Models
{
    public partial class User: IUser
    {
        public User(IRegisterModel r)
        {
            Username = r.Username;
            Email = r.Email;
            Firstname = r.Firstname;
            Lastname = r.Lastname;
            Password = r.Password;
            RegistrationDate = r.RegistrationDate;
        }
    }
}
