﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WT.DataAccess.Models;

namespace WT.DataAccess.Repositories
{
    public class BaseRepository
    {
        protected readonly WTEntities _context;
        public BaseRepository()
        {
            _context = new WTEntities();
        }

        public virtual void Save()
        {

        }
    }
}
