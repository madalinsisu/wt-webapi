﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WT.Models;

namespace WT.DataAccess.Repositories
{
    public class CityRepository: BaseRepository
    {
        public CityRepository(): base()
        {
        }

        public IEnumerable<CityModel> GetCities()
        {
            var cities = _context.Cities.ToList();

            var result = cities.Select(x => new CityModel(x)).ToList();

            return result;
        }

        public IEnumerable<CityModel> GetCitiesFromState(int stateId)
        {
            var cities = _context.Cities.Where(x => x.StateId == stateId).ToList();

            var result = cities.Select(x => new CityModel(x)).ToList();

            return result;
        }
    }
}
