﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WT.Models;

namespace WT.DataAccess.Repositories
{
    public class ContactRepository: BaseRepository
    {
        public ContactRepository() : base()
        {
        }

        public bool AddContactModel(ContactModel model)
        {
            var contact = _context.Contacts.Create();
            contact.Content = model.Content;
            contact.Email = model.Email;
            contact.Name = model.Name;
            contact.Subject = model.Subject;
            _context.Contacts.Add(contact);

            return _context.SaveChanges() > 0;
        }
    }
}
