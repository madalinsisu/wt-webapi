﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WT.Models;

namespace WT.DataAccess.Repositories
{
    public class PlaceRepository: BaseRepository
    {
        public PlaceRepository() : base()
        {
        }

        public IEnumerable<PlaceModel> GetPlaces()
        {
            var places = _context.Places.ToList();

            var result = places.Select(x => new PlaceModel(x)).ToList();

            return result;
        }

        public IEnumerable<PlaceModel> GetPlacesFromCity(int cityId)
        {
            var places = _context.Places.Where(x => x.CityId == cityId).ToList();

            var result = places.Select(x => new PlaceModel(x)).ToList();

            return result;
        }

        public IEnumerable<PlaceModel> GetPlacesFromState(int stateId)
        {
            var places = _context.Places.Where(x => x.City.StateId == stateId).ToList();

            var result = places.Select(x => new PlaceModel(x)).ToList();

            return result;
        }
    }
}
