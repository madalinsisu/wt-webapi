﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WT.Models;

namespace WT.DataAccess.Repositories
{
    public class StateRepository: BaseRepository
    {
        public StateRepository() : base()
        {
        }

        public IEnumerable<StateModel> GetStates()
        {
            var states = _context.States.ToList();

            var result = states.Select(x => new StateModel(x)).ToList();

            return result;
        }
    }
}
