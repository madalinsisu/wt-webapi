﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WT.Common.Enums;
using WT.DataAccess.Models;
using WT.ModelAbstractions;
using WT.Models;

namespace WT.DataAccess.Repositories
{
    public class UserRepository: BaseRepository 
    {
        public UserRepository(): base()
        {
        }

        public UserDefaultInfo GetUser(int id)
        {
            var user = _context.Users.FirstOrDefault(x => x.Id == id);
            if(user != null)
            {
                var userModel = new UserModel(user);
                return userModel;
            }
            return null;
        }

        public UserModel GetUser(string username, string password)
        {
            var user = _context.Users.FirstOrDefault(x => x.Password == password && (x.Email.ToLower() == username.ToLower() || x.Username.ToLower() == username.ToLower()));

            return user != null ? new UserModel(user) : null;
        }
        
        public override void Save()
        {
            base.Save();
        }

        public UserModel Login(string username, string password)
        {
            var user = _context.Users.FirstOrDefault(x => x.Password == password && (x.Email.ToLower() == username.ToLower() || x.Username.ToLower() == username.ToLower()));

            return user != null ? new UserModel(user) : null;
        }

        public UserModel Register(IRegisterModel register)
        {
            if(register.ConfirmPassword != register.Password)
            {
                return null;
            }
            register.RegistrationDate = DateTime.Now;
            var user = new User(register);
            _context.Users.Add(user);
            _context.SaveChanges();
            var savedUser = Login(register.Username, register.Password);
            if(savedUser != null)
            {
                return new UserModel(savedUser);
            }
            return null;
        }

        public bool UserExists(string username, string email)
        {
            return _context.Users.FirstOrDefault(x => x.Username.ToLower() == username) != null
                || _context.Users.FirstOrDefault(x => x.Email.ToLower() == email.ToLower()) != null;
        }

        public ChangeDetailsResult UpdateUser(UserDefaultInfo user, int userId)
        {
            string lowerUsername = user.Username.ToLower();
            string lowerEmail = user.Email.ToLower();
            var existingUser = _context.Users.FirstOrDefault(x => (x.Email.ToLower().Equals(lowerEmail) || x.Username.ToLower().Equals(lowerUsername)) && x.Id != userId);
            if(existingUser != null)
            {
                return ChangeDetailsResult.InvalidUsernameOrEmail;
            }

            var dbUser = _context.Users.FirstOrDefault(x => x.Id == userId);
            if(dbUser != null)
            {
                dbUser.Email = user.Email;
                dbUser.Firstname = user.Firstname;
                dbUser.Lastname = user.Lastname;
                dbUser.Username = user.Username;
                _context.SaveChanges();
                return ChangeDetailsResult.Success;
            }
            return ChangeDetailsResult.None;
        }

        public ChangePasswordResult ChangePassword(ChangePasswordModel model, int userId)
        {
            var user = _context.Users.FirstOrDefault(x => x.Id == userId);
            if(user != null )
            {
                if(!model.NewPassword.Equals(model.ConfirmNewPassword))
                {
                    return ChangePasswordResult.NotMatch;
                }
                if(!model.OldPassword.Equals(user.Password))
                {
                    return ChangePasswordResult.WrongOldPassword;
                }
                user.Password = model.NewPassword;
                _context.SaveChanges();
                return ChangePasswordResult.Success;
            }
            return ChangePasswordResult.UserNotFound;
        }
    }
}
