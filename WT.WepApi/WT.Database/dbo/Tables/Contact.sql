﻿CREATE TABLE [dbo].[Contact] (
    [Id]      INT            IDENTITY (1, 1) NOT NULL,
    [Name]    NVARCHAR (128) NULL,
    [Email]   NVARCHAR (64)  NULL,
    [Subject] NVARCHAR (128) NULL,
    [Content] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED ([Id] ASC)
);

