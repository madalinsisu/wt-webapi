﻿CREATE TABLE [dbo].[Place] (
    [Id]     INT             IDENTITY (1, 1) NOT NULL,
    [Name]   NVARCHAR (1024) NOT NULL,
    [CityId] INT             NOT NULL,
    CONSTRAINT [PK_Place] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Place_City] FOREIGN KEY ([CityId]) REFERENCES [dbo].[City] ([Id])
);

