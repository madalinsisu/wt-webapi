﻿CREATE TABLE [dbo].[Reply] (
    [Id]      INT            IDENTITY (1, 1) NOT NULL,
    [PostId]  INT            NOT NULL,
    [Content] NVARCHAR (MAX) NOT NULL,
    [UserId]  INT            NOT NULL,
    [Date]    DATETIME       NOT NULL,
    CONSTRAINT [PK_Reply] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Reply_Post] FOREIGN KEY ([PostId]) REFERENCES [dbo].[Post] ([Id]),
    CONSTRAINT [FK_Reply_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id])
);

