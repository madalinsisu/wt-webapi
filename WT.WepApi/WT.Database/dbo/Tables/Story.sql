﻿CREATE TABLE [dbo].[Story] (
    [Id]      INT            IDENTITY (1, 1) NOT NULL,
    [UserId]  INT            NOT NULL,
    [PlaceId] INT            NULL,
    [CityId]  INT            NULL,
    [StateId] INT            NULL,
    [Content] NVARCHAR (MAX) NULL,
    [Date]    DATETIME       NOT NULL,
    CONSTRAINT [PK_Story] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Story_City] FOREIGN KEY ([CityId]) REFERENCES [dbo].[City] ([Id]),
    CONSTRAINT [FK_Story_Place] FOREIGN KEY ([PlaceId]) REFERENCES [dbo].[Place] ([Id]),
    CONSTRAINT [FK_Story_State] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id]),
    CONSTRAINT [FK_Story_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id])
);

