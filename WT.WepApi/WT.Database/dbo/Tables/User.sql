﻿CREATE TABLE [dbo].[User] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [Firstname]        NVARCHAR (64) NOT NULL,
    [Lastname]         NVARCHAR (64) NOT NULL,
    [Username]         NVARCHAR (64) NOT NULL,
    [Email]            NVARCHAR (64) NOT NULL,
    [Password]         NVARCHAR (64) NOT NULL,
    [RegistrationDate] DATETIME      NOT NULL,
    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([Id] ASC)
);

