﻿create view vwPost
as
select Id, Content, [Date], [Type] = 1
from Post

union

select Id, Content, [Date], [Type] = 2
from Story