﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WT.ModelAbstractions
{
    public interface ICity
    {
        int Id { get; set; }
        int StateId { get; set; }
        IState State { get; }
        string Name { get; set; }
    }
}
