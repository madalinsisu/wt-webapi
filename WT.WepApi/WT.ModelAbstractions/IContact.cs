﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WT.ModelAbstractions
{
    public interface IContact
    {
        int Id { get; set; }
        string Name { get; set; }
        string Email { get; set; }
        string Subject { get; set; }
        string Content { get; set; }
    }
}
