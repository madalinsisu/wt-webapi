﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WT.ModelAbstractions
{
    public interface IPlace
    {
        int Id { get; set; }
        int CityId { get; set; }
        ICity City { get; }
        string Name { get; set; }
    }
}
