﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WT.ModelAbstractions
{
    public interface IRegisterModel
    {
        string Firstname { get; set; }
        string Lastname { get; set; }
        string Username { get; set; }
        string Email { get; set; }
        string Password { get; set; }
        string ConfirmPassword { get; set; }
        DateTime RegistrationDate { get; set; }
    }
}
