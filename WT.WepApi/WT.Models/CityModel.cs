﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WT.ModelAbstractions;

namespace WT.Models
{
    public class CityModel
    {
        public int Id { get; set; }
        public int StateId { get; set; }
        public StateModel State { get; set; }
        public string Name { get; set; }

        public CityModel()
        {

        }

        public CityModel(ICity city)
        {
            Id = city.Id;
            StateId = city.StateId;
            Name = city.Name;
            State = new StateModel(city.State);
        }
    }
}
