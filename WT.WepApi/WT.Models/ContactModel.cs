﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WT.ModelAbstractions;

namespace WT.Models
{
    public class ContactModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }

        public ContactModel()
        {
        }

        public ContactModel(IContact c)
        {
            Id = c.Id;
            Name = c.Name;
            Email = c.Email;
            Subject = c.Subject;
            Content = c.Content;
        }
    }
}
