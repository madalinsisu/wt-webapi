﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WT.Models
{
    public class LoginModel
    {
        [MaxLength(50)]
        public string Username { get; set; }
        
        [MaxLength(500)]
        public string Password { get; set; }
    }
}
