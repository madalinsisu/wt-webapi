﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WT.ModelAbstractions;

namespace WT.Models
{
    public class PlaceModel
    {
        public int Id { get; set; }
        public int CityId { get; set; }
        public CityModel City { get; set; }
        public string Name { get; set; }

        public PlaceModel()
        {
        }

        public PlaceModel(IPlace place)
        {
            Id = place.Id;
            CityId = place.CityId;
            Name = place.Name;
            City = new CityModel(place.City);
        }
    }
}
