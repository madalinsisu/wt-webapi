﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WT.Models
{
    public class ReplyModel
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public string Content { get; set; }
    }
}
