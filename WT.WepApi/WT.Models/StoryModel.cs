﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WT.Models
{
    public class StoryModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int? PlaceId { get; set; }
        public int? CityId { get; set; }
        public int? StateId { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
    }
}
