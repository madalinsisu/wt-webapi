﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WT.ModelAbstractions;

namespace WT.Models
{
    public class UserDefaultInfo: IUserDefaultInfo
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }

        public UserDefaultInfo(IUserDefaultInfo user)
        {
            Id = user.Id;
            Firstname = user.Firstname;
            Lastname = user.Lastname;
            Username = user.Username;
            Email = user.Email;
        }

        public UserDefaultInfo(UserModel user)
        {
            Id = user.Id;
            Firstname = user.Firstname;
            Lastname = user.Lastname;
            Username = user.Username;
            Email = user.Email;
        }

        public UserDefaultInfo()
        {
        }
    }
}
