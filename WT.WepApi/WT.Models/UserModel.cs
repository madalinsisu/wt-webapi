﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WT.ModelAbstractions;

namespace WT.Models
{
    public class UserModel: UserDefaultInfo, IUser
    {
        public string Password { get; set; }
        public DateTime RegistrationDate { get; set; }

        public UserModel(IUser user)
        {
            Id = user.Id;
            Firstname = user.Firstname;
            Lastname = user.Lastname;
            Username = user.Username;
            Email = user.Email;
            Password = user.Password;
            RegistrationDate = user.RegistrationDate;
        }

        public UserModel(IRegisterModel user)
        {
            Firstname = user.Firstname;
            Lastname = user.Lastname;
            Username = user.Username;
            Email = user.Email;
            Password = user.Password;
            RegistrationDate = user.RegistrationDate;
        }

        public UserModel()
        {

        }
    }
}
