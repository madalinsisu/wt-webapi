﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WT.DataAccess.Repositories;
using WT.Models;

namespace WT.Services
{
    public class AreasService
    {
        private CityRepository _cityRepository;
        private PlaceRepository _placeRepository;
        private StateRepository _stateREpository;

        public AreasService()
        {
            _cityRepository = new CityRepository();
            _placeRepository = new PlaceRepository();
            _stateREpository = new StateRepository();
        }

        public IEnumerable<CityModel> GetAllCities()
        {
            var result = _cityRepository.GetCities();

            return result;
        }

        public IEnumerable<PlaceModel> GetPlacesFromCity(int cityId)
        {
            var result = _placeRepository.GetPlacesFromCity(cityId);

            return result;
        }

        public IEnumerable<PlaceModel> GetPlacesState(int stateId)
        {
            var result = _placeRepository.GetPlacesFromState(stateId);

            return result;
        }
    }
}
