﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WT.DataAccess.Repositories;
using WT.Models;

namespace WT.Services
{
    public class ContactService
    {
        private ContactRepository _contactRepository;

        public ContactService()
        {
            _contactRepository = new ContactRepository();
        }

        public bool AddContact(ContactModel model)
        {
            return _contactRepository.AddContactModel(model);
        }
    }
}
