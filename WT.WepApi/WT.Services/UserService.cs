﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WT.Common.Constants;
using WT.DataAccess.Repositories;
using WT.Models;
using WT.ModelAbstractions;
using WT.Common.Enums;

namespace WT.Services
{
    public class UserService
    {
        private readonly UserRepository _repository;
        public UserService()
        {
            _repository = new UserRepository();
        }

        public UserDefaultInfo GetUser(int id)
        {
            var user = _repository.GetUser(id);
            var result = new UserDefaultInfo(user);
            return result;
        }

        public UserModel Login(string username, string password)
        {
            return _repository.Login(username, password);
        }

        public UserModel Register (RegisterModel registerModel)
        {
            return _repository.Register(registerModel);
        }

        public bool UserExists(string username, string email)
        {
            return _repository.UserExists(username, email);
        }

        public ChangeDetailsResult UpdateUser(UserDefaultInfo user, int userId)
        {
            return _repository.UpdateUser(user, userId);
        }

        public ChangePasswordResult ChangePassword(ChangePasswordModel model, int userId)
        {
            return _repository.ChangePassword(model, userId);
        }
    }
}
