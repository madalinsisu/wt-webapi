﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WT.WepApi.Constants
{
    public class Routes
    {
        public const string Home = "";
        public const string Authenticate = "api/authenticate";
        public const string Cities = "api/areas/cities";
        public const string PlacesFromCity = "api/areas/places/city/{cityId}";
        public const string PlacesFromState = "api/areas/places/state/{stateId}";
        public const string Register = "api/register";
        public const string Login = "api/login";
    }
}