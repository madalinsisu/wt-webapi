﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WT.Models;
using WT.Services;
using WT.WepApi.Constants;

namespace WT.WepApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AreasController : ApiController
    {
        private readonly AreasService _areasService;

        public AreasController()
        {
            _areasService = new AreasService();
        }

        [HttpGet]
        [Route(Routes.Cities)]
        public IHttpActionResult GetCities()
        {
            var result = _areasService.GetAllCities();

            return Ok(result);
        }

        [HttpGet]
        [Route(Routes.PlacesFromCity)]
        public IHttpActionResult GetPlacesFromCity(int cityId)
        {
            var result = _areasService.GetPlacesFromCity(cityId);

            return Ok(result);
        }

        [HttpGet]
        [Route(Routes.PlacesFromState)]
        public IHttpActionResult GetPlacesFromState(int stateId)
        {
            var result = _areasService.GetPlacesState(stateId);

            return Ok(result);
        }
    }
}
