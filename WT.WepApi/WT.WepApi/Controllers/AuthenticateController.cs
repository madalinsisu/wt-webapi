﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WT.Common.Constants;
using WT.Models;
using WT.Services;
using WT.WepApi.Constants;
using WT.WepApi.Helpers;


namespace WT.WepApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AuthenticateController : BaseApiController
    {
        private UserService _service;
        public AuthenticateController()
        {
            _service = new UserService();
        }
        
        [HttpPost]
        [Route(Routes.Authenticate)]
        [AllowAnonymous]
        public IHttpActionResult Authenticate([FromBody]LoginModel loginModel)
        {
            // Find user and generate token
            var user = _service.Login(loginModel.Username, loginModel.Password);

            //If the returned user ID is 0, set the NotFound status code
            if (user == null)
            {
                return Content(HttpStatusCode.NotFound, WT.Common.Constants.Constants.Messages.ExceptionInvalidCredentials);
            }

            var helper = new AuthenticateHelper();
            return Ok(helper.GenerateToken(loginModel.Username, user.Id, 1000));
        }

        [HttpPost]
        [Route(Routes.Register)]
        public IHttpActionResult Register([FromBody] RegisterModel registerModel)
        {
            if(_service.UserExists(registerModel.Username, registerModel.Email))
            {
                return Content(HttpStatusCode.NotFound, WT.Common.Constants.Constants.Messages.ExceptionInvalidEmailOrUsername);
            }
            var user = _service.Register(registerModel);
            if(user == null)
            {
                return Content(HttpStatusCode.NotFound, WT.Common.Constants.Constants.Messages.ExceptionPassword);
            }

            var helper = new AuthenticateHelper();
            return Ok(helper.GenerateToken(registerModel.Username, user.Id, 1000));
        }
    }
}
