﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WT.Common.Enums;
using WT.Common.Helpers;
using WT.Models;
using WT.Services;

namespace WT.WepApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ContactController : BaseApiController
    {
        private ContactService _contactService;

        public ContactController()
        {
            _contactService = new ContactService();
        }

        [HttpPost]
        [Route("api/contact")]
        public IHttpActionResult AddContact([FromBody] ContactModel model)
        {
            var result = _contactService.AddContact(model);
            var res = result ? ContactResult.Success : ContactResult.Failed;
            var httpResult = EnumHelper.GetDescription(res);
            return Content((HttpStatusCode)httpResult.HttpStatusCode, httpResult.Description);
        }
    }
}
