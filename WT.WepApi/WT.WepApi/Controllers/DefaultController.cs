﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WT.WepApi.Constants;

namespace WT.WepApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DefaultController : ApiController
    {
        [HttpGet]
        [Route(Routes.Home)]
        public IHttpActionResult Index()
        {
            return Ok("WT service 2018");
        }
    }
}
