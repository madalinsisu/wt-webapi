﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;
using WT.Common.Enums;
using WT.Common.Helpers;
using WT.Models;
using WT.Services;
using WT.WepApi.Filters;
using WT.WepApi.Models.Authorization;

namespace WT.WepApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [AuthenticateFilterAttribute]
    public class UsersController : BaseApiController
    {
        private UserService _service;
        public UsersController()
        {
            _service = new UserService();
        }

        [HttpGet]
        [Route("api/profile")]
        public IHttpActionResult GetUser()
        {
            var identity = Thread.CurrentPrincipal.Identity as Identity;
            if(identity != null)
            {
                var user = _service.GetUser(identity.UserId);
                if(user.Id != identity.UserId)
                {
                    //TO DO: return user information without sensitive data
                    return Content(HttpStatusCode.NonAuthoritativeInformation, WT.Common.Constants.Constants.Messages.ExceptionUnauthorized);
                }
                return Ok(user);
            }
            return NotFound();
        }

        [HttpPut]
        [Route("api/profile")]
        public IHttpActionResult UpdateUser([FromBody]UserDefaultInfo user)
        {
            var identity = Thread.CurrentPrincipal.Identity as Identity;
            if(identity != null)
            {
                var result = _service.UpdateUser(user, identity.UserId);
                var httpResult = EnumHelper.GetDescription(result);
                return Content((HttpStatusCode)httpResult.HttpStatusCode, httpResult.Description);
            }
            return NotFound();
        }

        [HttpPost]
        [Route("api/changepassword")]
        public IHttpActionResult ChangePassword([FromBody]ChangePasswordModel model)
        {
            var identity = Thread.CurrentPrincipal.Identity as Identity;
            if(identity != null)
            {
                var result = _service.ChangePassword(model, identity.UserId);
                var httpResult = EnumHelper.GetDescription(result);
                return Content((HttpStatusCode)httpResult.HttpStatusCode, httpResult.Description);
            }
            return NotFound();
        }
    }
}
