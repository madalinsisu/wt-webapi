﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using WT.WepApi.Helpers;
using WT.WepApi.Models.Authorization;

namespace WT.WepApi.Filters
{
    public class AuthenticateFilterAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext filterContext)
        {
            if (!IsUserAuthorized(filterContext))
            {
                throw new UnauthorizedAccessException();
            }
            base.OnAuthorization(filterContext);
        }

        private bool IsUserAuthorized(HttpActionContext filterContext)
        {
            var authHeader = FetchFromHeader(filterContext);
            if (authHeader != null)
            {
                var auth = new AuthenticateHelper();
                JwtSecurityToken userPayloadToken = auth.GenerateUserClaimFromJwt(authHeader);

                if (userPayloadToken != null)
                {
                    var identity = auth.PopulateUserIdentity(userPayloadToken);
                    string[] roles = { "All" };
                    var genericPrincipal = new GenericPrincipal(identity, roles);
                    Thread.CurrentPrincipal = genericPrincipal;
                    var authenticationIdentity = Thread.CurrentPrincipal.Identity as Identity;
                    if (authenticationIdentity != null && !string.IsNullOrEmpty(authenticationIdentity.UserName))
                    {
                        authenticationIdentity.UserId = identity.UserId;
                        authenticationIdentity.UserName = identity.UserName;
                    }
                    return true;
                }
            }
            return false;
        }

        private string FetchFromHeader(HttpActionContext actionContext)
        {
            string requestToken = null;

            var authRequest = actionContext.Request.Headers.Authorization;
            if (authRequest != null)
            {
                requestToken = authRequest.Parameter;
            }

            return requestToken;
        }
    }
}