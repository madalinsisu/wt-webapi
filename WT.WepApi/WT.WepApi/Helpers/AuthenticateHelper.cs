﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using WT.WepApi.Models.Authorization;

namespace WT.WepApi.Helpers
{
    public class AuthenticateHelper
    {
        readonly SecurityKey _signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("GQDstc21ewfffffffffffFiwDffVvVBrk"));

        public string GenerateToken(string username, int userId, int expireMinutes = 20)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var signinCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256Signature);
            var now = DateTime.UtcNow;
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                        {
                        new Claim(ClaimTypes.Name, username),
                        new Claim(ClaimTypes.NameIdentifier, userId.ToString())
                    }),

                Expires = now.AddMinutes(Convert.ToInt32(expireMinutes)),

                SigningCredentials = signinCredentials,
                Audience = "wtcommunity",
                Issuer = "self"
            };

            var stoken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(stoken);

            return token;
        }

        public JwtSecurityToken GenerateUserClaimFromJwt(string authToken)
        {
            var tokenValidationParameters = new TokenValidationParameters()
            {
                ValidAudiences = new string[] { "wtcommunity" },
                ValidIssuers = new string[] { "self" },
                IssuerSigningKey = _signingKey,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken validatedToken;

            try
            {
                tokenHandler.ValidateToken(authToken, tokenValidationParameters, out validatedToken);

            }
            catch (Exception ex)
            {
                return null;
            }

            return validatedToken as JwtSecurityToken;
        }
        
        public Identity PopulateUserIdentity(JwtSecurityToken userPayloadToken)
        {
            string name = ((userPayloadToken)).Claims.FirstOrDefault(m => m.Type == "unique_name").Value;

            int userId;
            string user = ((userPayloadToken)).Claims.FirstOrDefault(m => m.Type == "nameid").Value;
            int.TryParse(user, out userId);

            return new Identity(name) { UserId = userId, UserName = name };
        }
    }
}