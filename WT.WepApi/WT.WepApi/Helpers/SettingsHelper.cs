﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace WT.WepApi.Helpers
{
    public class SettingsHelper
    {
        public static string JwtCommunicationKey
        {
            get
            {
                return ConfigurationManager.AppSettings["JwtCommunicationKey"];
            }
        }

        public static string JwtTokenAudience
        {
            get
            {
                return ConfigurationManager.AppSettings["JwtTokenAudience"];
            }
        }

        public static string JwtTokenIssuer
        {
            get
            {
                return ConfigurationManager.AppSettings["JwtTokenIssuer"];
            }
        }

        public static int JwtTokenExpireMinutes
        {
            get
            {
                var minutes = 0;
                int.TryParse(ConfigurationManager.AppSettings["JwtTokenExpireMinutes"], out minutes);
                return minutes;
            }
        }
    }
}