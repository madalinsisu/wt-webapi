﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace WT.WepApi.Models.Authorization
{
    public class Identity : GenericIdentity
    {
        public string UserName { get; set; }
        public int UserId { get; set; }

        public Identity(string userName) : base(userName)
        {
            UserName = userName;
        }
    }
}